import sbt._

class Project(info: ProjectInfo) extends DefaultProject(info) {

  val scalaTools = ScalaToolsSnapshots

  override def libraryDependencies = Set(
      "org.codehaus.jackson" % "jackson-mapper-asl" % "1.7.1"
    , "com.googlecode.scalaz" % "scalaz-core_2.8.0" % "5.0"
    , "org.scalatest" % "scalatest" % "1.2" % "test"
  )
}

